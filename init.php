<?php
/*
Plugin Name: Marketo Connector - Public Version
Plugin URI: http://hooshmarketing.com.au/internal/wordpress-plugin-documentation/
Description: Marketo Munchkin Embedding, Form shortcodes to send to marketo.
Version: 1.0.0
Author: Hoosh Marketing
Author URI: http://hooshmarketing.com.au/internal/wordpress-plugin-documentation/
Text Domain: marketoconnector-public
*/
include_once ( 'marketo-connector.php' );
include_once ( 'marketo-admin.php' );
$marketoConnector = new MarketoConnector();

if (is_admin()){
	$marketoAdmin = new MarketoAdmin( $marketoConnector );
}
register_activation_hook( __FILE__, array($marketoConnector,'activate'));
register_deactivation_hook( __FILE__, array($marketoConnector,'deactivate'));